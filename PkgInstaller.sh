#!/bin/bash

dpkg -l | grep $pkg >/dev/null 2>&1

if [ $? -eq 0 ];
then
        echo "Package  is installed!"
else
        echo "Installing the Package"
        sudo apt install $pkg 
fi
